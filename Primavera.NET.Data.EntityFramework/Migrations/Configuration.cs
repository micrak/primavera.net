namespace Primavera.NET.Data.EntityFramework.Migrations
{
    #region Usings
    using System.Data.Entity.Migrations;

    using Primavera.NET.Data.EntityFramework.UnitOfWork;
    using Primavera.NET.Data.UnitOfWork;

    #endregion

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        #region Constructors
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
            this.ContextKey = "Primavera.NET.Data.EntityFramework.UnitOfWork.DatabaseContext";
        } 
        #endregion

        #region Override methods
        protected override void Seed(DatabaseContext context)
        {
            IUnitOfWork unitOfWork = new EntityFrameworkUnitOfWork(context);
            ISeed seed = new DefaultSeed(unitOfWork);
            seed.Begin();
        } 
        #endregion
    }
}
