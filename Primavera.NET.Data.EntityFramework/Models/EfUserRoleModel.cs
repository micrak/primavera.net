﻿namespace Primavera.NET.Data.EntityFramework.Models
{
    #region Methods

    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Primavera.NET.Data.Models; 
    #endregion

    [Table("UsersRoles")]
    public class EfUserRoleModel : UserRoleModel
    {
        #region Properties
        [Key]
        public override int Id { get; set; }

        [ForeignKey("Role")]
        public override int RoleId { get; set; }

        public virtual EfRoleModel Role { get; set; }

        [ForeignKey("User")]
        public override int UserId { get; set; }

        public virtual EfUserModel User { get; set; } 
        #endregion
    }
}
