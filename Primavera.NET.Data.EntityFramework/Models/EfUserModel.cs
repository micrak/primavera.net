﻿namespace Primavera.NET.Data.EntityFramework.Models
{
    #region Usings
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Primavera.NET.Data.Models; 
    #endregion

    [Table("Users")]
    public class EfUserModel : UserModel
    {
        [Key]
        public override int Id { get; set; }
    }
}
