﻿namespace Primavera.NET.Data.EntityFramework.Models
{
    #region Usings
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Primavera.NET.Data.Models; 
    #endregion

    [Table("Messages")]
    public class EfMessageModel : MessageModel
    {
        #region Properties
        [Key]
        public override int Id { get; set; }
        #endregion
    }
}
