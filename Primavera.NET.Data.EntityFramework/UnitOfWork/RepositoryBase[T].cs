﻿namespace Primavera.NET.Data.EntityFramework.UnitOfWork
{
    #region Usings

    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    using Primavera.NET.Data.Models;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;    
    using Primavera.NET.Localization;

    #endregion

    public abstract class RepositoryBase<T, TProxy> : IRepository<T>
        where T : ModelBase
        where TProxy : ModelBase
    {
        #region Constructors
        protected RepositoryBase(DatabaseContext databaseDatabaseContext, DbSet<TProxy> databaseSet)
        {
            ValidationHelper.IsNotNull(databaseDatabaseContext, "databaseDatabaseContext");
            ValidationHelper.IsNotNull(databaseSet, "databaseSet");

            this.DatabaseContext = databaseDatabaseContext;
            this.DatabaseSet = databaseSet;
        }
        #endregion

        #region Properties
        protected DatabaseContext DatabaseContext { get; private set; }

        protected DbSet<TProxy> DatabaseSet { get; private set; }
        #endregion

        #region IRepository<T> methods
        public abstract T CreateNew();

        public virtual void Add(T entity)
        {
            ValidationHelper.IsNotNull(entity, "entity");

            DbEntityEntry entry = this.DatabaseContext.Entry(entity as TProxy);

            if (entry.State != EntityState.Detached)
            {
                entry.State = EntityState.Added;
            }
            else
            {
                this.DatabaseSet.Add(entity as TProxy);
            }
        }

        public virtual void Update(T entity)
        {
            ValidationHelper.IsNotNull(entity, "entity");

            TProxy entry = this.DatabaseSet.Find(entity.Id);

            if (entry == null)
            {
                throw new UnitOfWorkException(Strings.StringErrorTheEntityXHasBeenNotFound.FormatText(entity.Id).Dot());
            }

            this.DatabaseContext.Entry(entity).CurrentValues.SetValues(entity);
        }

        public void Delete(T entity)
        {
            ValidationHelper.IsNotNull(entity, "entity");

            DbEntityEntry entry = this.DatabaseContext.Entry(entity);

            if (entry == null)
            {
                throw new UnitOfWorkException(Strings.StringErrorTheEntityXHasBeenNotFound.FormatText(entity.Id).Dot());
            }

            if (entry.State != EntityState.Deleted)
            {
                entry.State = EntityState.Deleted;
            }
            else
            {
                this.DatabaseSet.Attach(entity as TProxy);
                this.DatabaseSet.Remove(entity as TProxy);
            }
        } 
        #endregion
    }
}
