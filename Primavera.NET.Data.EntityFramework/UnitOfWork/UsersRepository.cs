﻿namespace Primavera.NET.Data.EntityFramework.UnitOfWork
{
    #region Usings

    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using Primavera.NET.Data.EntityFramework.Models;
    using Primavera.NET.Data.Models;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;

    #endregion

    public class UsersRepository : RepositoryBase<UserModel, EfUserModel>, IUsersRepository
    {
        #region Private variables
        private static readonly object Lock = new object();
        #endregion

        #region Constructors
        public UsersRepository(DatabaseContext databaseContext, DbSet<EfUserModel> databaseSet)
            : base(databaseContext, databaseSet)
        {            
        }
        #endregion

        #region Override methods
        public override UserModel CreateNew()
        {
            return new EfUserModel();
        } 
        #endregion

        #region IUsersRepository methods
        public UserModel GetBy(string email, string password)
        {
            lock (Lock)
            {
                return this.DatabaseSet.FirstOrDefault(user => user.Email.Equals(email) && user.Password.Equals(password)); 
            }
        }

        public UserModel GetByEmailOrNickname(string email, string nickname)
        {
            lock (Lock)
            {
                return this.DatabaseSet.FirstOrDefault(user => user.Email.Equals(email) || user.Nickname.Equals(nickname)); 
            }
        }

        public void AddRoles(int userId, IEnumerable<RoleType> roles)
        {
            lock (Lock)
            {
                ValidationHelper.IsNotNull(roles, "roles");

                var roleModels = this.DatabaseContext.Roles.Where(role => roles.Contains(role.RoleType)).ToList();

                foreach (EfRoleModel loopRoleModel in roleModels)
                {
                    var userRoleModel = new EfUserRoleModel();
                    userRoleModel.UserId = userId;
                    userRoleModel.RoleId = loopRoleModel.Id;
                    this.DatabaseContext.UsersRoles.Add(userRoleModel);
                } 
            }
        }

        public IList<UserModel> GetBy(IEnumerable<int> ids)
        {
            lock (Lock)
            {
                ValidationHelper.IsNotNull(ids, "ids");

                if (!ids.Any())
                {
                    return new List<UserModel>();
                }

                return this.DatabaseSet.Where(g => ids.Contains(g.Id)).AsEnumerable<UserModel>().ToList(); 
            }
        }

        public UserModel GetBy(int id)
        {
            lock (Lock)
            {
                return this.DatabaseSet.FirstOrDefault(g => g.Id == id); 
            }
        }

        public IList<UserModel> GetAll()
        {
            lock (Lock)
            {
                return this.DatabaseSet.AsEnumerable<UserModel>().ToList(); 
            }
        }
        #endregion           
    }
}
