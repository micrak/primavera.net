﻿namespace Primavera.NET.Data.EntityFramework.UnitOfWork
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading;

    using Primavera.NET.Data.EntityFramework.Models;
    using Primavera.NET.Data.Models;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;

    #endregion

    public class MessagesRepository : RepositoryBase<MessageModel, EfMessageModel>, IMessagesRepository
    {
        #region Private variables
        private static readonly object Lock = new object();
        #endregion

        #region Constructors
        public MessagesRepository(DatabaseContext databaseContext, DbSet<EfMessageModel> databaseSet)
            : base(databaseContext, databaseSet)
        {
        }
        #endregion

        #region Override methods
        public override MessageModel CreateNew()
        {
            return new EfMessageModel();
        }
        #endregion

        #region Methods
        public IList<MessageModel> GetPrivateMessages(int userId1, int userId2, int count)
        {
            lock (Lock)
            {
                Expression<Func<EfMessageModel, bool>> where =
                        g =>
                            (g.SenderUserId == userId1 && g.RecipientUserId == userId2) ||
                            (g.SenderUserId == userId2 && g.RecipientUserId == userId1);

                var query = this.DatabaseSet.Where(where).OrderBy(g => g.CreatedAt);
                int elementsCount = query.Count();

                return
                    query
                    .GetLast(count, elementsCount)
                    .AsEnumerable<MessageModel>()
                    .ToList();
            }
        }

        public IList<MessageModel> GetPrivateMessaegs(int userId1, int userId2, int count, int beforeMessageId)
        {
            lock (Lock)
            {
                Expression<Func<EfMessageModel, bool>> where =
                        g =>
                            ((g.SenderUserId == userId1 && g.RecipientUserId == userId2) ||
                            (g.SenderUserId == userId2 && g.RecipientUserId == userId1)) &&
                            g.Id < beforeMessageId;

                var query = this.DatabaseSet.Where(where).OrderBy(g => g.CreatedAt);

                int elementsCount = query.Count();

                return query
                    .GetLast(count, elementsCount)
                    .AsEnumerable<MessageModel>()
                    .ToList();
            }
        }

        public IList<MessageModel> GetChatMessages(int count)
        {
            lock (Lock)
            {
                var query = this.DatabaseSet.Where(g => g.RecipientUserId == null).OrderBy(g => g.CreatedAt);

                int elementsCount = query.Count();

                return query
                    .GetLast(count, elementsCount)
                    .AsEnumerable<MessageModel>()
                    .ToList();
            }
        }

        public IList<MessageModel> GetChatMessages(int count, int beforeMessageId)
        {
            lock (Lock)
            {
                var query = this.DatabaseSet
                    .Where(g => g.RecipientUserId == null && g.Id < beforeMessageId)
                    .OrderBy(g => g.CreatedAt);

                int elementsCount = query.Count();

                return
                    query
                    .GetLast(count, elementsCount)
                    .AsEnumerable<MessageModel>()
                    .ToList();
            }
        }
        #endregion
    }
}
