﻿namespace Primavera.NET.Data.EntityFramework.UnitOfWork
{
    #region Usings

    using System;
    using System.Data.Common;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    using Primavera.NET.Data.EntityFramework.Models;
    #endregion

    public class DatabaseContext : DbContext
    {
        #region Private variables

        private static readonly Type SqlServerType = typeof(System.Data.Entity.SqlServer.SqlFunctions);
        #endregion

        #region Constructors
        public DatabaseContext()
            : base("Primavera")
        {            
        }

        public DatabaseContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            this.Configuration.EnsureTransactionsForFunctionsAndCommands = true;
        }
        #endregion

        #region Properties
        public virtual DbSet<EfUserModel> Users { get; set; }

        public virtual DbSet<EfRoleModel> Roles { get; set; }

        public virtual DbSet<EfUserRoleModel> UsersRoles { get; set; }

        public virtual DbSet<EfMessageModel> Messages { get; set; }
        #endregion
    }
}
