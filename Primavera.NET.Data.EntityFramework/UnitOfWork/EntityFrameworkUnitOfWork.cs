﻿namespace Primavera.NET.Data.EntityFramework.UnitOfWork
{
    #region Usings
    using System;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;

    #endregion

    public class EntityFrameworkUnitOfWork : IUnitOfWork
    {
        #region Constants

        private const string ConstConnectionStringName = "Primavera";
        #endregion

        #region Private variables
        private readonly DatabaseContext databaseContext;

        private readonly Lazy<IUsersRepository> lazyUsersRepository;
        private readonly Lazy<IRolesRepository> lazyRolesRepository;
        private readonly Lazy<IMessagesRepository> lazyMessagesRepository;
        #endregion

        #region Constructors

        public EntityFrameworkUnitOfWork(DatabaseContext databaseContext)
        {
            ValidationHelper.IsNotNull(databaseContext, "databaseContext");

            this.databaseContext = databaseContext;

            this.lazyRolesRepository = new Lazy<IRolesRepository>(() => new RolesRepository(this.databaseContext, this.databaseContext.Roles));
            this.lazyUsersRepository = new Lazy<IUsersRepository>(() => new UsersRepository(this.databaseContext, this.databaseContext.Users));
            this.lazyMessagesRepository = new Lazy<IMessagesRepository>(() => new MessagesRepository(this.databaseContext, this.databaseContext.Messages));
        }

        public EntityFrameworkUnitOfWork()
            : this(new DatabaseContext(ConstConnectionStringName))
        {
        }
        #endregion

        #region IUnitOfWork properties
        public IUsersRepository Users
        {
            get
            {
                return this.lazyUsersRepository.Value;
            }
        }

        public IRolesRepository Roles
        {
            get
            {
                return this.lazyRolesRepository.Value;
            }
        }

        public IMessagesRepository Messages
        {
            get
            {
                return this.lazyMessagesRepository.Value;
            }
        }
        #endregion

        #region IUnitOfWork methods
        public void SaveChanges()
        {
            this.databaseContext.SaveChanges();
        } 
        #endregion
    }
}
