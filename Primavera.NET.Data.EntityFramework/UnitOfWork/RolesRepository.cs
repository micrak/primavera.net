﻿namespace Primavera.NET.Data.EntityFramework.UnitOfWork
{
    #region Usings

    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using Primavera.NET.Data.EntityFramework.Models;
    using Primavera.NET.Data.Models;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;

    #endregion

    public class RolesRepository : RepositoryBase<RoleModel, EfRoleModel>, IRolesRepository
    {
        #region Private variables
        private static readonly object Lock = new object();
        #endregion

        #region Constructors
        public RolesRepository(DatabaseContext databaseContext, DbSet<EfRoleModel> databaseSet)
            : base(databaseContext, databaseSet)
        {
        }
        #endregion

        #region Override methods
        public override RoleModel CreateNew()
        {
            return new EfRoleModel();
        }
        #endregion

        #region IRolesRepository methods
        public IList<RoleModel> GetBy(int userId)
        {
            lock (Lock)
            {
                var query = this.DatabaseContext.UsersRoles.Where(g => g.UserId == userId).Select(g => g.Role);

                return query.AsEnumerable().OfType<RoleModel>().ToList(); 
            }
        }

        public IDictionary<int, IList<RoleModel>> GetBy(IEnumerable<int> userIds)
        {
            lock (Lock)
            {
                ValidationHelper.IsNotNull(userIds, "userIds");

                if (!userIds.Any())
                {
                    return new Dictionary<int, IList<RoleModel>>();
                }

                var userRoles = this.DatabaseContext.UsersRoles.Where(g => userIds.Contains(g.UserId)).ToList();

                var result = new Dictionary<int, IList<RoleModel>>();

                foreach (var loopUserRole in userRoles)
                {
                    IList<RoleModel> roles;
                    if (!result.TryGetValue(loopUserRole.UserId, out roles))
                    {
                        roles = new List<RoleModel>();
                        result[loopUserRole.UserId] = roles;
                    }

                    result[loopUserRole.UserId].Add(loopUserRole.Role);
                }

                return result; 
            }
        }

        public void RemoveUserFromAllRoles(int userid)
        {
            lock (Lock)
            {
                var entities = this.DatabaseContext.UsersRoles.Where(g => g.UserId == userid).ToList();

                foreach (var entity in entities)
                {
                    this.DatabaseContext.UsersRoles.Remove(entity);
                } 
            }
        }
        #endregion        
    }
}
