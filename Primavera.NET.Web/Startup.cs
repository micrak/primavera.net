﻿namespace Primavera.NET.Web
{
    #region Usings
    using Owin; 
    #endregion

    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            Bootstrapper.Start(appBuilder);
        }
    }
}