﻿namespace Primavera.NET.Web
{
    using System.Collections.Generic;
    using System.Linq;

    using Primavera.NET.Framework;

    public class DefaultNotificationService : INotificationService
    {
        #region Private variables
        private readonly List<Notification> notifications;
        #endregion

        #region Constructors
        public DefaultNotificationService()
        {
            this.notifications = new List<Notification>();
        }
        #endregion

        #region INotificationService methods
        public void Error(string message)
        {
            this.MyAddNotification(NotificationType.Error, message);
        }

        public void Information(string message)
        {
            this.MyAddNotification(NotificationType.Information, message);
        }

        public IEnumerable<Notification> GetNotifications(bool clear)
        {
            var result = this.notifications.ToList();

            if (clear)
            {
                this.notifications.Clear();
            }

            return result;
        } 
        #endregion

        #region My methods
        private void MyAddNotification(NotificationType notificationType, string message)
        {
            this.notifications.Add(new Notification { NotificationType = notificationType, Message = message });
        }
        #endregion
    }
}