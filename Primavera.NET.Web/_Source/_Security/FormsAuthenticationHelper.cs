﻿namespace Primavera.NET.Web
{
    #region Usings
    using System;
    using System.Web;
    using System.Web.Security;
    using Newtonsoft.Json;
    using Primavera.NET.Framework;     
    #endregion

    public static class FormsAuthenticationHelper
    {
        public static void WritePrincipal(CustomPrincipal customPrincipal)
        {
            ValidationHelper.IsNotNull(customPrincipal, "customPrincipal");

            var serializationData = new CustomIdentitySerializationData(customPrincipal.CustomIdentity);

            string userData = JsonConvert.SerializeObject(serializationData);
            var authTicket = new FormsAuthenticationTicket(1, customPrincipal.Identity.Name, DateTime.Now, DateTime.Now.AddMinutes(30), false, userData);
            
            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);    
        
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void ReadPrincipal()
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie == null)
            {
                return;
            }

            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

            if (ticket == null)
            {
                return;
            }

            var serializationData = JsonConvert.DeserializeObject<CustomIdentitySerializationData>(ticket.UserData);

            if (serializationData == null || serializationData.Email == null)
            {
                return;
            }

            HttpContext.Current.User = new CustomPrincipal(serializationData.ToCustomIdentity());
        }

        public static void ClearPrincipal()
        {
            FormsAuthentication.SignOut();
        }
    }
}