﻿namespace Primavera.NET.Web
{
    #region Usings
    using System.Linq;
    using System.Security.Principal;

    using Primavera.NET.Framework; 
    #endregion

    public class CustomPrincipal : IPrincipal
    {
        #region Private variables
        private readonly CustomIdentity customIdentity;
        #endregion

        #region Constructors
        public CustomPrincipal(CustomIdentity identity)
        {
            ValidationHelper.IsNotNull(identity, "identity");

            this.customIdentity = identity;
        }
        #endregion

        #region IPrincipal properties

        public IIdentity Identity
        {
            get
            {
                return this.customIdentity;
            }
        }

        public CustomIdentity CustomIdentity
        {
            get
            {
                return this.customIdentity;
            }
        }

        #endregion

        #region Methods
        public static CustomPrincipal Empty()
        {
            return new CustomPrincipal(new CustomIdentity());
        }
        #endregion

        #region IPrincipal methods

        public bool IsInRole(string role)
        {
            return this.customIdentity.Roles.Any(g => g.ToString().Equals(role));
        }

        #endregion
    }
}