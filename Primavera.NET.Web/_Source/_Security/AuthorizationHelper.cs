﻿namespace Primavera.NET.Web
{   
    #region Usigns

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Mvc;

    using Microsoft.Ajax.Utilities;

    using Primavera.NET.Framework;

    #endregion
    
    public static class AuthorizationHelper
    {
        public static bool Authorize(HttpContextBase httpContext, string roles)
        {
            ValidationHelper.IsNotNull(httpContext, "httpContext");

            if (httpContext.User == null || httpContext.User.Identity == null
                || !httpContext.User.Identity.IsAuthenticated)
            {
                return false;
            }

            if (string.IsNullOrEmpty(roles))
            {
                return true;
            }

            var splittedRoles = roles.Split(',');

            if (!splittedRoles.Any())
            {
                return true;
            }
            
            foreach (var loopRole in splittedRoles)
            {
                if (httpContext.User.IsInRole(loopRole))
                {
                    return true;
                }
            }

            return false;
        }        
    }
}