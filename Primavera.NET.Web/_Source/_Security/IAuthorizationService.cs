﻿namespace Primavera.NET.Web
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Security;
    using Primavera.NET.Data.Models;
    #endregion

    public interface IAuthorizationService
    {
        CustomPrincipal Authorize(string email, string password);

        void Register(string email, string password, string nickname, IEnumerable<RoleType> roles);

        bool UserExists(string email, string nickname);
    }
}
