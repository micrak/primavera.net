﻿namespace Primavera.NET.Web
{
    using System.Collections.Generic;
    using System.Linq;
    using Primavera.NET.Data.Models;
    using Primavera.NET.Framework;

    public class CustomIdentitySerializationData
    {
        #region Constructors
        public CustomIdentitySerializationData(CustomIdentity customIdentity)
        {
            ValidationHelper.IsNotNull(customIdentity, "customIdentity");

            this.Email = customIdentity.Email;
            this.Nickname = customIdentity.Nickname;
            this.UserId = customIdentity.UserId;
            this.Roles = customIdentity.Roles.ToList();
        }

        public CustomIdentitySerializationData()
        {
        }
        #endregion

        #region Properties
        public string Email { get; set; }

        public string Nickname { get; set; }

        public int UserId { get; set; }

        public IList<RoleType> Roles { get; set; }
        #endregion

        #region Methods
        public CustomIdentity ToCustomIdentity()
        {
            return new CustomIdentity(this.Email, this.Nickname, this.UserId, this.Roles);
        }
        #endregion
    }
}