﻿namespace Primavera.NET.Web
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Primavera.NET.Data.Models;

    #endregion

    public class MvcAuthorizeAttribute : AuthorizeAttribute
    {
        #region Override methods
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!AuthorizationHelper.Authorize(filterContext.HttpContext, this.Roles))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "PermissionDenied" }));
            }
        }
        #endregion
    }
}