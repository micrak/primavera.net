﻿namespace Primavera.NET.Web
{
    #region Usings
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;
    using Primavera.NET.Data.Models;
    using Primavera.NET.Framework;
    #endregion

    public class CustomIdentity : IIdentity
    {
        #region Constructors

        public CustomIdentity()
        {       
            this.Roles = new List<RoleType>();
        }

        public CustomIdentity(string email, string nickname, int userId, IEnumerable<RoleType> roles)
        {
            ValidationHelper.IsNotNull(email, "email");
            ValidationHelper.IsNotNull(nickname, "nickname");            
            ValidationHelper.IsNotNull(roles, "roles");

            this.Email = email;
            this.Nickname = nickname;
            this.UserId = userId;            
            this.Roles = roles.ToList();
        }
        #endregion

        #region Properties
        public string Email { get; private set; }

        public string Nickname { get; private set; }

        public int UserId { get; private set; }

        public IList<RoleType> Roles { get; private set; }
        #endregion

        #region IIdentity properties
        public string AuthenticationType { get; set; }

        public bool IsAuthenticated
        {
            get
            {
                return !string.IsNullOrEmpty(this.Email);
            }
        }

        public string Name
        {
            get
            {
                return this.Nickname;
            }
        }
        #endregion
    }
}