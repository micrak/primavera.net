﻿namespace Primavera.NET.Web
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;
    using System.Web.Caching;
    using System.Web.Security;

    using Newtonsoft.Json;

    using Primavera.NET.Data.Models;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;
    using Primavera.NET.Localization;

    #endregion

    public class DefaultAuthorizationService : IAuthorizationService
    {        
        #region Private variables
        private readonly IUnitOfWork unitOfWork;
        #endregion

        #region Constructors
        public DefaultAuthorizationService(IUnitOfWork unitOfWork)
        {
            ValidationHelper.IsNotNull(unitOfWork, "unitOfWork");

            this.unitOfWork = unitOfWork;
        }
        #endregion

        #region IAuthorizationService methods
        public CustomPrincipal Authorize(string email, string password)
        {
            ValidationHelper.IsNotNull(email, "email");
            ValidationHelper.IsNotNull(password, "password");

            UserModel userModel = this.unitOfWork.Users.GetBy(email, PasswordsHelper.Hash(password));

            if (userModel == null)
            {
                return null;
            }
            else
            {
                IList<RoleModel> roles = this.unitOfWork.Roles.GetBy(userModel.Id);
                IList<RoleType> roleTypes = roles.Select(g => g.RoleType).ToList();
                return new CustomPrincipal(new CustomIdentity(userModel.Email, userModel.Nickname, userModel.Id, roleTypes));                
            }           
        }

        public void Register(string email, string password, string nickname, IEnumerable<RoleType> roles)
        {
            ValidationHelper.IsNotNull(email, "email");
            ValidationHelper.IsNotNull(password, "password");
            ValidationHelper.IsNotNull(nickname, "nickname");
            
            UserModel userModel = this.unitOfWork.Users.CreateNew();
            userModel.Email = email;
            userModel.Nickname = nickname;
            userModel.Password = PasswordsHelper.Hash(password);
            this.unitOfWork.Users.Add(userModel);
            this.unitOfWork.SaveChanges();

            this.unitOfWork.Users.AddRoles(userModel.Id, roles);
            this.unitOfWork.SaveChanges();
        }

        public bool UserExists(string email, string nickname)
        {
            ValidationHelper.IsNotNull(email, "email");
            ValidationHelper.IsNotNull(nickname, "nickname");

            if (this.unitOfWork.Users.GetByEmailOrNickname(email, nickname) == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}