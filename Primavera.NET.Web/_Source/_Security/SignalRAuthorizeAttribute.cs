﻿namespace Primavera.NET.Web
{    
    #region Usings
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;

    #endregion

    public class SignalRAuthorizeAttribute : AuthorizeAttribute
    {
        public override bool AuthorizeHubConnection(HubDescriptor hubDescriptor, IRequest request)
        {
            var context = request.GetHttpContext();
            
            return AuthorizationHelper.Authorize(context, this.Roles);
        }
    }
}