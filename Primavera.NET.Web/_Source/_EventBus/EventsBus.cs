﻿namespace Primavera.NET.Web
{    
    #region Usings

    using System;

    using Primavera.NET.Data.Models;
    using Primavera.NET.Framework;

    #endregion

    public class EventsBus : IEventsBus
    {
        #region Events
        public event EventHandler<OnUserDisconnectRequestedEventArgs> OnUserDisconnectRequested
        {
            add
            {
                InternalOnUserDisconnectRequested += value;
            }

            remove
            {
                InternalOnUserDisconnectRequested -= value;
            }
        }

        public event EventHandler<EventArgs> OnUsersListReloadRequested
        {
            add
            {
                InternalOnUsersListReloadRequested += value;
            }

            remove
            {
                InternalOnUsersListReloadRequested -= value;
            }
        }

        private static event EventHandler<OnUserDisconnectRequestedEventArgs> InternalOnUserDisconnectRequested;

        private static event EventHandler<EventArgs> InternalOnUsersListReloadRequested;
        
        #endregion

        #region Methods

        public void RaiseOnUsersListReloadRequested(object sender, EventArgs eventArgs)
        {
            ValidationHelper.IsNotNull(sender, "sender");
            ValidationHelper.IsNotNull(eventArgs, "eventArgs");

            var handler = InternalOnUsersListReloadRequested;

            if (handler != null)
            {
                handler(sender, eventArgs);
            }
        }

        public void RaiseOnUserDisconnectRequested(object sender, OnUserDisconnectRequestedEventArgs args)
        {
            ValidationHelper.IsNotNull(sender, "sender");
            ValidationHelper.IsNotNull(args, "args");

            var handler = InternalOnUserDisconnectRequested;

            if (handler != null)
            {
                handler(sender, args);
            }
        }
        #endregion
    }
}