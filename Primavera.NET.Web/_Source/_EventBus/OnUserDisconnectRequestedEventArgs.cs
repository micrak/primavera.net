﻿namespace Primavera.NET.Web
{
    #region Usings
    using System;
    #endregion

    public class OnUserDisconnectRequestedEventArgs : EventArgs
    {
        #region Constructors
        public OnUserDisconnectRequestedEventArgs(int userId)
        {
            this.UserId = userId;
        }
        #endregion

        #region Properties
        public virtual int UserId { get; private set; }
        #endregion
    }
}