﻿namespace Primavera.NET.Web
{
    #region Usings
    using System;

    using Primavera.NET.Data.Models; 
    #endregion

    public interface IEventsBus
    {
        #region Events
        event EventHandler<OnUserDisconnectRequestedEventArgs> OnUserDisconnectRequested;

        event EventHandler<EventArgs> OnUsersListReloadRequested;
        #endregion

        #region Methods
        void RaiseOnUserDisconnectRequested(object sender, OnUserDisconnectRequestedEventArgs args);

        void RaiseOnUsersListReloadRequested(object sender, EventArgs eventArgs);

        #endregion
    }
}
