﻿namespace Primavera.NET.Web
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    using Primavera.NET.Framework;

    public static class TempDataDictionaryExtensions
    {
        #region Methods
        public static void AddNotifications(this TempDataDictionary tempDataDictionary, IEnumerable<Notification> notifications)
        {
            ValidationHelper.IsNotNull(tempDataDictionary, "tempDataDictionary");
            ValidationHelper.IsNotNull(notifications, "notifications");

            List<Notification> tempDataNotifications = MyGetTempDataObject(
                tempDataDictionary,
                TempDataConstants.Notification,
                () => new List<Notification>());

            tempDataNotifications.AddRange(notifications);
        }

        public static IEnumerable<Notification> GetNotifications(this TempDataDictionary tempDataDictionary)
        {
            ValidationHelper.IsNotNull(tempDataDictionary, "tempDataDictionary");

            return MyGetTempDataObject(
                tempDataDictionary,
                TempDataConstants.Notification,
                () => new List<Notification>());
        }
        #endregion

        #region My methods
        private static T MyGetTempDataObject<T>(this TempDataDictionary tempData, string key, Func<T> factory)
        {
            object tempDataObject;

            if (!tempData.TryGetValue(key, out tempDataObject))
            {
                tempDataObject = (object)factory();
                tempData[key] = tempDataObject;
            }

            return (T)tempDataObject;
        } 
        #endregion
    }
}