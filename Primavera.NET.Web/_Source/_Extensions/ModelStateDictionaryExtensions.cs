﻿namespace Primavera.NET.Web
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using Primavera.NET.Framework;
    #endregion

    public static class ModelStateDictionaryExtensions
    {
        public static void Merge(this ModelStateDictionary modelState, Dictionary<string, string> errors)
        {
            ValidationHelper.IsNotNull(modelState, "modelState");
            ValidationHelper.IsNotNull(errors, "errors");

            foreach (var loopError in errors)
            {
                modelState.AddModelError(loopError.Key, loopError.Value);
            }
        }

        public static void RemoveErrors(this ModelStateDictionary modelState, string key)
        {
            ValidationHelper.IsNotNull(modelState, "modelState");
            ValidationHelper.IsNotNull(key, "key");

            if (modelState.ContainsKey(key))
            {
                modelState[key].Errors.Clear();
            }
        }

        public static void RemoveErrors<TViewModel>(this ModelStateDictionary modelState, Expression<Func<TViewModel, object>> lambdaExpression)
        {
            ValidationHelper.IsNotNull(modelState, "modelState");
            ValidationHelper.IsNotNull(lambdaExpression, "lambdaExpression");

            modelState.RemoveErrors(ExpressionHelper.GetExpressionText(lambdaExpression));
        }

        public static void AddModelError<TViewModel>(this ModelStateDictionary modelState, Expression<Func<TViewModel, object>> lambdaExpression, string error)
        {
            ValidationHelper.IsNotNull(modelState, "modelState");
            ValidationHelper.IsNotNull(lambdaExpression, "lambdaExpression");
            ValidationHelper.IsNotNull(error, "error");

            modelState.AddModelError(ExpressionHelper.GetExpressionText(lambdaExpression), error);
        }
    }
}