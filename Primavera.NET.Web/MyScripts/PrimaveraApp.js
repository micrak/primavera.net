﻿var primaveraApp = angular.module('primaveraApp', ['emoticonizeFilter']);

primaveraApp.directive('scrollToLast', ['$', '$rootScope', function ($, $rootScope) {
    return function (scope, element) {
        $rootScope.$on('scrollRequested', function (e, delay) {
            var go = function () {
                var target = $(element).children().last();

                if (target.html() != null) {
                    $(element).scrollTo(target, delay);
                }
            };
            setTimeout(go, 10);
        });
    };
}]);

primaveraApp.service('$', function () {
    return jQuery;
});

primaveraApp.service('linq', function () {
    return Enumerable;
});

primaveraApp.service('logger', function () {
    var myPrintMessage = function (type, message) {
        var date = new Date();
        var dateString = date.getTime();
        console.log('[' + dateString + '] [' + type + '] ' + message);
    };

    return {
        info: function (message) {
            myPrintMessage('INFO', message);
        },
        error: function (message) {
            myPrintMessage('ERROR', message);
        }
    };
});

primaveraApp.service('hubService', ['$', '$rootScope', 'logger', function ($, $rootScope, logger) {
    var primaveraHub = $.connection.primaveraHub;
    var client = primaveraHub.client;
    var server = primaveraHub.server;

    client.receivedDisconnectRequest = function () {
        logger.info('Received disconnect request');
        $.connection.hub.stop();
    };

    client.receivedUsersList = function (users) {
        logger.info('Received users list');
        $rootScope.$emit('receivedUsersList', users);
    };

    client.receivedChatMessages = function (messages) {
        logger.info('Received messages');
        $rootScope.$emit('receivedChatMessages', messages);
    };

    client.receivedMessage = function (message) {
        logger.info('Received message');
        $rootScope.$emit('receivedMessage', message);
    }

    client.receivedUserChanged = function (user) {
        logger.info('Received user changed');
        $rootScope.$emit('receivedUserChanged', user);
    };

    var connect = function () {
        logger.info('Connecting');
        $.connection.hub.start().done(function () {
            logger.info('Connected');
            $rootScope.$emit('connected');
        }).fail(function (error) {
        });
    };

    connect();

    $.connection.hub.disconnected(function () {
        try {
            $.connection.hub.stop();
        } catch (exception) {
        }

        logger.error('Disconnected');
        $rootScope.$emit('disconnected');
    });

    $.connection.hub.error(function () {
        logger.error('There are some problems with connection');
    });

    $.connection.hub.reconnecting(function () {
        logger.error('Reconnecting');
        $rootScope.$emit('reconnecting');
    });

    $.connection.hub.reconnected(function () {
        logger.error('Reconnected');
        $rootScope.$emit('reconnected');
    });

    return {
        getUsers: function () {
            server.getUsers();
        },
        getResources: function () {
            return server.getResources();
        },
        getCurrentUser: function () {
            return server.getCurrentUser();
        },
        getChatMessages: function (count, beforeMessageId) {
            return server.getChatMessages(count, beforeMessageId);
        },
        getPrivateMessages: function (userId1, userId2, count, beforeMessageId) {
            return server.getPrivateMessages(userId1, userId2, count, beforeMessageId);
        },
        sendMessage: function (message, recipientUserId) {
            server.sendMessage(message, recipientUserId);
        },
        getUnreadMessages: function () {
            server.getUnreadMessages();
        },
        setStatus: function (statusMessage) {
            server.setStatus(statusMessage);
        }
    };
}]);

primaveraApp.controller('ChatController', ['hubService', '$rootScope', '$scope', 'logger', 'linq', function (hubService, $rootScope, $scope, logger, linq, $sce) {
    $scope.users = [];
    $scope.currentUser = {};
    $scope.settings = {
        elementsToTake: 5,
    };
    $scope.resources = null;
    $scope.tabs = [];
    $scope.tab = null;
    $scope.chatTab = null;
    $scope.messageModel = {};
    $scope.connected = true;
    $scope.loadcompleted = false;

    $scope.setTab = function (tab) {
        $scope.tab = tab;
        $scope.tab.unreadMessages = 0;
        $scope.$emit('scrollRequested', 10);
    };

    $scope.isActive = function (tab) {
        return $scope.tab === tab;
    };

    $scope.isUserAvailable = function (recipientUserId) {
        var user = linq.From($scope.users).Where(function (g) { return g.id == recipientUserId; }).FirstOrDefault();
        if (user != null && user.status == 'Available') {
            return true;
        }

        return false;
    };

    $scope.provideTab = function (recipientUserId) {
        var recipientTab = linq.From($scope.tabs).Where(function (g) { return g.recipientUserId === recipientUserId; }).FirstOrDefault();
        if (recipientTab == null) {
            var recipientUser = linq.From($scope.users).Where(function (g) { return g.id === recipientUserId; }).FirstOrDefault();
            recipientTab = {
                title: recipientUser.nickname,
                type: 'user',
                canUserClose: true,
                messages: [],
                unreadMessages: 0,
                recipientUserId: recipientUserId,
                sendMessage: function () {
                    $scope.sendMessage(recipientUserId);
                },
                getOlderMessages: function () {
                    if (recipientTab.messages.length < 0) {
                        return;
                    }
                    var messagesPromise = hubService.getPrivateMessages($scope.currentUser.id, recipientUserId, $scope.settings.elementsToTake, recipientTab.messages[0].id);
                    messagesPromise.done(function (receivedMessages) {
                        $scope.$apply(function () {
                            recipientTab.messages = linq.From(receivedMessages).Union(recipientTab.messages).ToArray();
                        });
                    });
                }
            };
            $scope.tabs.push(recipientTab);

            var promise = hubService.getPrivateMessages($scope.currentUser.id, recipientUser.id, $scope.settings.elementsToTake);
            promise.done(function (messages) {
                logger.info('Received private messages');
                $scope.$apply(function () {
                    recipientTab.messages = linq.From(messages).Union(recipientTab.messages).ToArray();
                });
                $rootScope.$emit('scrollRequested', 100);
            });
        }
        return recipientTab;
    };

    $scope.deleteTab = function (tab) {
        var index = $scope.tabs.indexOf(tab);
        $scope.tabs = linq.From($scope.tabs).Where(function (g) { return g !== tab; }).ToArray();
        $scope.setTab($scope.tabs[index - 1]);
    };

    $rootScope.$on('connected', function () {
        $scope.$apply(function () {
            $scope.connected = true;
        });

        logger.info('Requesting current user');
        hubService.getCurrentUser().done(function (user) {

            $scope.currentUser = user;

            logger.info('Requesting resources');
            hubService.getResources().done(function (resources) {
                $scope.resources = resources;

                logger.info('Requesting members and messages');
                hubService.getUsers();
                hubService.getChatMessages($scope.settings.elementsToTake);
                hubService.getUnreadMessages();
            });
        });
    });

    $rootScope.$on('reconnecting', function () {
        $scope.$apply(function () {
            $scope.connected = false;
        });
    });

    $rootScope.$on('disconnected', function () {
        $scope.$apply(function () {
            $scope.connected = false;
        });
    });

    $rootScope.$on('reconnected', function () {
        $scope.$apply(function () {
            $scope.connected = true;
        });
    });

    $rootScope.$on('receivedChatMessages', function (e, messages) {
        $scope.$apply(function () {
            $scope.loadcompleted = true;

            $scope.chatTab = {
                title: $scope.resources.StringChat,
                type: 'chat',
                userCanClose: false,
                messages: messages !== null ? messages : [],
                unreadMessages: 0,
                recipientUserId: null,
                sendMessage: function () {
                    $scope.sendMessage(null);
                },
                getOlderMessages: function () {
                    if ($scope.chatTab.messages.length <= 0) {
                        return;
                    }

                    var promise = hubService.getChatMessages($scope.settings.elementsToTake, $scope.chatTab.messages[0].id);
                    promise.done(function (receivedMessages) {
                        $rootScope.$apply(function () {
                            $scope.chatTab.messages = linq.From(receivedMessages).Union($scope.chatTab.messages).ToArray();
                        });
                    });
                },
            };
            $scope.tabs.push($scope.chatTab);
            $scope.setTab($scope.chatTab);

            $rootScope.$emit('scrollRequested', 10);
        });
    });

    $rootScope.$on('receivedMessage', function (e, message) {
        $scope.$apply(function () {
            var tab = null;
            if (message.recipientUserId !== null) {
                if (message.recipientUserId === $scope.currentUser.id) {
                    tab = $scope.provideTab(message.senderUserId);
                } else {
                    tab = $scope.provideTab(message.recipientUserId);
                }
            } else {
                tab = $scope.chatTab;
            }

            tab.messages.push(message);
            if ($scope.tab !== tab && message.senderUserId !== $scope.currentUser.id) {
                tab.unreadMessages++;
            }

            $rootScope.$emit('scrollRequested', 500);
        });
    });

    $scope.sendMessage = function (recipientUserId) {
        hubService.sendMessage($scope.messageModel.content, recipientUserId);
        $scope.messageModel = {};
    };
}]);

primaveraApp.controller('UsersController', ['$rootScope', '$scope', 'linq', 'logger', 'hubService', function ($rootScope, $scope, linq, logger, hubService) {
    var updateUser = function (user) {
        $scope.$parent.$apply(function () {
            if (user === null) {
                return;
            }

            logger.info("User status changed");

            var filtered = linq
                .From($scope.$parent.users)
                .Where(function (g) { return g.id !== user.id; }).ToArray();

            filtered.push(user);

            $scope.$parent.users = sortUsers(filtered);
        });
    };

    var sortUsers = function (users) {
        return linq.From(users).OrderBy(function (g) { return g.statusCode; }).ToArray();
    }

    this.userModel = $scope.$parent.currentUser;

    this.isMe = function (user) {
        return $scope.$parent.currentUser.id === user.id;
    };

    this.saveStatus = function () {
        hubService.setStatus($scope.$parent.currentUser.statusMessage);
        alert($scope.$parent.resources.StringYourStatusHasBeenChangedSuccessFully);
    };

    this.openTab = function (recipientUserId) {
        if (recipientUserId === $scope.$parent.currentUser.id) {
            return;
        }
        $scope.$parent.tab = $scope.$parent.provideTab(recipientUserId);
    };

    $rootScope.$on('receivedUserChanged', function (e, user) {
        updateUser(user);
    });

    $rootScope.$on('receivedUsersList', function (e, users) {
        $scope.$parent.$apply(function () {
            if (users !== null && users.length > 0) {
                $scope.$parent.users = sortUsers(users);
            }
        });
    });
}]);