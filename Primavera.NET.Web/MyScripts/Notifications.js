﻿$(document).ready(function() {
    var notifications = $('.container-notifications').children();

    var typesMap = {
        'Notification': 'success',
        'Warning': 'warning',
        'Error': 'danger'
    };

    if (notifications.length > 0) {
        for (var i=0; i<notifications.length; i++) {
            var notificationType = $(notifications[i]).attr('data-notification-type');
            var notificationText = $(notifications[i]).attr('data-notification-message');

            $(notifications[i]).notify({
                message: { text: notificationText },
                type: typesMap[notificationType]
            }).show();
        }
    }    
});