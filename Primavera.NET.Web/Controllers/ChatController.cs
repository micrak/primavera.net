﻿namespace Primavera.NET.Web.Controllers
{    
    #region Usings

    using System;
    using System.Web.Mvc;

    using Primavera.NET.Data.Models;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;
   
    #endregion

    [MvcAuthorize(Roles = "User")]
    public class ChatController : ControllerBase
    {
        #region Constructors
        public ChatController(IUnitOfWork unitOfWork, INotificationService notificationService)
            : base(unitOfWork, notificationService)
        {
        } 
        #endregion

        #region Methods

        public ActionResult Index()
        {
            return this.View();
        }
        #endregion
    }
}