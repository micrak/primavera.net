﻿namespace Primavera.NET.Web.Controllers
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Primavera.NET.Data.Models;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;
    using Primavera.NET.Localization;
    using Primavera.NET.Web.Models;

    #endregion

    [MvcAuthorize(Roles = "Admin")]
    public class AdminUsersController : ControllerBase
    {
        #region Private variables

        private readonly IMapper mapper;

        private readonly IEventsBus eventBus;
        #endregion

        #region Constructors
        public AdminUsersController(IUnitOfWork unitOfWork, INotificationService notificationService, IMapper mapper, IEventsBus eventBus)
            : base(unitOfWork, notificationService)
        {
            ValidationHelper.IsNotNull(mapper, "mapper");
            ValidationHelper.IsNotNull(eventBus, "eventBus");

            this.mapper = mapper;
            this.eventBus = eventBus;
        }
        #endregion

        #region Methods (actions)

        public ActionResult Index()
        {
            var users = this.UnitOfWork.Users.GetAll();
            var usersRoles = this.UnitOfWork.Roles.GetBy(users.Select(g => g.Id));

            var items = this.mapper.Map<UserModel, AdminUsersItemViewModel>(users);

            foreach (var loopItem in items)
            {
                IList<RoleModel> roles;
                if (usersRoles.TryGetValue(loopItem.Id, out roles))
                {
                    loopItem.Roles.AddRange(roles.Select(g => g.RoleType));
                }
            }

            var model = new AdminUsersIndexViewModel(items);

            this.InitalizeViewModel(model);

            return this.View(model);
        }

        [HttpGet]
        public ActionResult Delete(int userId)
        {
            if (userId == this.User.CustomIdentity.UserId)
            {
                this.NotificationService.Error(Strings.StringYouCannotDeleteYourSelf.Dot());
                return this.RedirectToAction("Index");
            }          

            this.UnitOfWork.Roles.RemoveUserFromAllRoles(userId);
            UserModel userModel = this.UnitOfWork.Users.GetBy(userId);

            if (userModel == null)
            {
                return this.RedirectResourceNotFound();
            }

            this.UnitOfWork.Users.Delete(userModel);
            this.UnitOfWork.SaveChanges();

            this.eventBus.RaiseOnUserDisconnectRequested(this, new OnUserDisconnectRequestedEventArgs(userId));
            this.eventBus.RaiseOnUsersListReloadRequested(this, EventArgs.Empty);

            this.NotificationService.Information(Strings.StringTheUserHasBeenDeleted.Dot());
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int userId)
        {
            UserModel userModel = this.UnitOfWork.Users.GetBy(userId);

            if (userModel == null)
            {
                return this.RedirectResourceNotFound();
            }

            AdminUsersItemViewModel model = this.mapper.Map<AdminUsersItemViewModel>(userModel);
            var roleModels = this.UnitOfWork.Roles.GetBy(userId);
            model.Roles.AddRange(roleModels.Select(g => g.RoleType));

            this.InitalizeViewModel(model);

            return this.View(model);
        }

        [HttpPost]
        public ActionResult Edit(AdminUsersItemViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                this.NotificationService.Error(Strings.StringSomeFieldsAreNotFilledCorrectly.Dot());
            }

            if (model.Id == this.User.CustomIdentity.UserId && !model.Roles.Contains(RoleType.Admin))
            {
                this.NotificationService.Error(Strings.StringYouCannotChangeYourXRole.FormatText(RoleType.Admin).Dot());
                this.ModelState.AddModelError<AdminUsersItemViewModel>(g => g.Roles, string.Empty.Space());
            }

            UserModel userWithSameNickname = this.UnitOfWork.Users.GetByEmailOrNickname(string.Empty, model.Nickname);
            if (userWithSameNickname != null && userWithSameNickname.Id != model.Id)
            {
                this.ModelState.AddModelError<AdminUsersItemViewModel>(g => g.Nickname, Strings.StringThisNicknameIsNotUnique.Dot());
            }
            
            if (!this.ModelState.IsValid)
            {
                this.NotificationService.Error(Strings.StringSomeFieldsAreNotFilledCorrectly.Dot());
                this.InitalizeViewModel(model);
                return this.View(model);
            }

            UserModel user = this.UnitOfWork.Users.GetBy(model.Id);

            if (user == null)
            {
                return this.RedirectResourceNotFound();
            }

            user.Nickname = model.Nickname;
            user.StatusMessage = model.StatusMessage;
            
            this.UnitOfWork.Users.Update(user);
            this.UnitOfWork.Roles.RemoveUserFromAllRoles(user.Id);
            this.UnitOfWork.Users.AddRoles(user.Id, model.Roles.Distinct());
            this.UnitOfWork.SaveChanges();

            this.eventBus.RaiseOnUsersListReloadRequested(this, EventArgs.Empty);

            this.NotificationService.Information(Strings.StringTheUserHasBeenChanged.Dot());

            return this.RedirectToAction("Index");
        }
        #endregion
    }
}