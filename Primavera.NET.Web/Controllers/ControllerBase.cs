﻿namespace Primavera.NET.Web.Controllers
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;
    using Primavera.NET.Web.Models;

    #endregion

    public abstract class ControllerBase : Controller
    {
        #region Constructors
        protected ControllerBase(IUnitOfWork unitOfWork, INotificationService notificationService)
        {
            ValidationHelper.IsNotNull(unitOfWork, "unitOfWork");
            ValidationHelper.IsNotNull(notificationService, "notificationService");

            this.UnitOfWork = unitOfWork;
            this.NotificationService = notificationService;
        }
        #endregion

        #region Properties

        protected virtual new CustomPrincipal User
        {
            get
            {
                var customPrincipal = HttpContext.User as CustomPrincipal;

                if (customPrincipal == null)
                {
                    customPrincipal = CustomPrincipal.Empty();
                }

                return customPrincipal;
            }
        }

        protected IUnitOfWork UnitOfWork { get; private set; }

        protected INotificationService NotificationService { get; private set; }
        #endregion

        #region Methods
        protected virtual ViewModelBase InitalizeViewModel(ViewModelBase viewModelBase)
        {
            return viewModelBase;
        }

        protected virtual ActionResult RedirectResourceNotFound()
        {
            return this.RedirectToAction("ResourceNotFound", "Error");
        }
        #endregion

        #region Override methods

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            this.TempData.AddNotifications(this.NotificationService.GetNotifications(true));
            base.OnActionExecuted(filterContext);
        }

        #endregion
    }
}