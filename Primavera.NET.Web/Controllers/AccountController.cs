﻿namespace Primavera.NET.Web.Controllers
{
    #region Usings

    using System;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;

    using Primavera.NET.Data;
    using Primavera.NET.Data.Models;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;
    using Primavera.NET.Localization;
    using Primavera.NET.Web.Models;
    #endregion

    public class AccountController : ControllerBase
    {
        #region Private variables
        private readonly IAuthorizationService authorizationService;

        private readonly IEventsBus eventBus;
        #endregion

        #region Constructors
        public AccountController(IUnitOfWork unitOfWork, INotificationService notificationService, IAuthorizationService authorizationService, IEventsBus eventBus)
            : base(unitOfWork, notificationService)
        {
            ValidationHelper.IsNotNull(authorizationService, "authorizationService");
            ValidationHelper.IsNotNull(eventBus, "eventBus");

            this.authorizationService = authorizationService;
            this.eventBus = eventBus;
        }
        #endregion

        #region Methods (actions)
        [HttpGet]
        public ActionResult Index()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return this.RedirectToAction("Index", "Chat");
            }
            else
            {
                object viewModel = this.InitalizeViewModel(new SignInViewModel());
                return this.View("Index", viewModel);
            }
        }

        [HttpPost]
        public ActionResult SignIn(SignInViewModel signInViewModel)
        {
            if (this.ModelState.IsValid)
            {
                CustomPrincipal customPrincipal = this.authorizationService.Authorize(signInViewModel.Email, signInViewModel.Password);

                if (customPrincipal == null)
                {
                    this.ModelState.AddModelError<SignInViewModel>(g => g.Email, Strings.StringTheValueXIsIncorrect.FormatText(signInViewModel.Email).Dot());
                    this.ModelState.AddModelError<SignInViewModel>(g => g.Password, Strings.StringTheValueXIsIncorrect.FormatText(signInViewModel.Password).Dot());

                    this.NotificationService.Error(Strings.StringAuthorizationFailed.Dot());

                    this.InitalizeViewModel(signInViewModel);
                    return this.View("Index", signInViewModel);
                }

                FormsAuthenticationHelper.WritePrincipal(customPrincipal);
            }

            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult SignUp()
        {
            object viewModel = this.InitalizeViewModel(new SignUpViewModel());
            return this.View(viewModel);
        }

        [HttpPost]
        public ActionResult SignUp(SignUpViewModel signUpViewModel)
        {
            if (!Regex.IsMatch(signUpViewModel.Email, RegularExpressions.EmailPattern))
            {
                this.ModelState.AddModelError<SignUpViewModel>(g => g.Email, Strings.StringTheValueXIsIncorrect.FormatText(signUpViewModel.Email).Dot());
            }

            if (!signUpViewModel.Password.Equals(signUpViewModel.ConfirmPassword))
            {
                this.ModelState.AddModelError<SignUpViewModel>(g => g.Password, Strings.StringPasswordsDoNotMatch.Dot());
                this.ModelState.AddModelError<SignUpViewModel>(g => g.ConfirmPassword, Strings.StringPasswordsDoNotMatch.Dot());
            }

            if (this.authorizationService.UserExists(signUpViewModel.Email, signUpViewModel.Password))
            {
                this.NotificationService.Error(Strings.StringUserWithEmailXOrNicknameXAlreadyExists.FormatText(signUpViewModel.Email, signUpViewModel.Nickname).Dot());
                this.ModelState.AddModelError<SignUpViewModel>(g => g.Nickname, string.Empty.Space());
                this.ModelState.AddModelError<SignInViewModel>(g => g.Email, string.Empty.Space());
            }

            if (!this.ModelState.IsValid)
            {
                this.NotificationService.Error(Strings.StringSomeFieldsAreNotFilledCorrectly.Dot());
                this.InitalizeViewModel(signUpViewModel);
                return this.View("SignUp", signUpViewModel);
            }

            this.authorizationService.Register(signUpViewModel.Email, signUpViewModel.Password, signUpViewModel.Nickname, new[] { RoleType.User });

            this.eventBus.RaiseOnUsersListReloadRequested(this, EventArgs.Empty);

            this.NotificationService.Information(Strings.StringYourAccountHasBeenCreated.Dot());

            return this.RedirectToAction("Index");
        }

        public ActionResult SignOut()
        {
            this.eventBus.RaiseOnUserDisconnectRequested(this, new OnUserDisconnectRequestedEventArgs(this.User.CustomIdentity.UserId));
            FormsAuthenticationHelper.ClearPrincipal();
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ChangeNickname()
        {
            var userModel = this.UnitOfWork.Users.GetBy(this.User.CustomIdentity.UserId);

            if (userModel == null)
            {
                return this.RedirectResourceNotFound();
            }

            var model = new ChangeNicknameViewModel() { Nickname = userModel.Nickname };
            this.InitalizeViewModel(model);
            return this.View(model);
        }

        [HttpPost]
        public ActionResult ChangeNickname(ChangeNicknameViewModel model)
        {
            var userWithSameNickname = this.UnitOfWork.Users.GetByEmailOrNickname(string.Empty, model.Nickname);

            if (userWithSameNickname != null && userWithSameNickname.Id != this.User.CustomIdentity.UserId)
            {
                this.ModelState.AddModelError<ChangeNicknameViewModel>(g => g.Nickname, Strings.StringThisNicknameIsNotUnique.Dot());
            }

            if (!ModelState.IsValid)
            {
                this.NotificationService.Error(Strings.StringSomeFieldsAreNotFilledCorrectly.Dot());
                this.InitalizeViewModel(model);
                return this.View(model);
            }

            var user = this.UnitOfWork.Users.GetBy(this.User.CustomIdentity.UserId);
            user.Nickname = model.Nickname;
            this.UnitOfWork.Users.Update(user);
            this.UnitOfWork.SaveChanges();

            this.NotificationService.Information(Strings.StringYourNicknameHasBeenChangedSuccessfully.Dot());

            this.eventBus.RaiseOnUsersListReloadRequested(this, EventArgs.Empty);

            this.InitalizeViewModel(model);
            return this.View(model);
        }
        #endregion
    }
}