﻿namespace Primavera.NET.Web.Controllers
{
    #region Usings
    using System;
    using System.Configuration;
    using System.Web.Configuration;
    using System.Web.Mvc;

    using Primavera.NET.Localization;
    using Primavera.NET.Web.Models;

    #endregion

    public class ErrorController : Controller
    {
        #region Constants
        private const string ConstCustomErrorsSection = "system.web/customErrors";
        #endregion

        #region Private variables
        private static readonly CustomErrorsMode ErrorsMode;
        #endregion

        #region Constructors
        static ErrorController()
        {
            var section = ConfigurationManager.GetSection(ConstCustomErrorsSection) as CustomErrorsSection;

            if (section == null)
            {
                ErrorsMode = CustomErrorsMode.RemoteOnly;
            }
            else
            {
                ErrorsMode = section.Mode;
            }
        }
        #endregion

        #region Methods (actions)
        public ActionResult PermissionDenied()
        {
            return this.View();
        }

        public ActionResult ResourceNotFound()
        {
            return this.View();
        }

        public ActionResult Exception(int statusCode, Exception exception)
        {
            bool isDeveloperMode;

            switch (ErrorsMode)
            {
                case CustomErrorsMode.On:
                    isDeveloperMode = false;
                    break;
                case CustomErrorsMode.Off:
                    isDeveloperMode = true;
                    break;
                default:
                case CustomErrorsMode.RemoteOnly:
                    isDeveloperMode = Request.IsLocal;
                    break;
            }

            string message;

            switch (statusCode)
            {
                default:
                    message = string.Empty;
                    break;
                case 400:
                    message = Strings.StringBadRequest;
                    break;
                case 404:
                    message = Strings.StringPageNotFound;
                    break;
                case 500:
                    message = Strings.StringInternalServerError;
                    break;
            }

            this.Response.StatusCode = statusCode;

            var viewModel = new ErrorViewModel(statusCode, message, exception, isDeveloperMode);

            return this.View(viewModel);
        }
        #endregion
    }
}