﻿namespace Primavera.NET.Web
{
    #region Usings

    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    using Primavera.NET.Web.Controllers;

    #endregion

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);            
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            FormsAuthenticationHelper.ReadPrincipal();   
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            Server.ClearError();
            Response.TrySkipIisCustomErrors = true;

            var routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("action", "Exception");
            routeData.Values.Add("exception", exception);

            var httpException = exception as HttpException;

            if (httpException == null)
            {
                routeData.Values.Add("statusCode", 500);
            }
            else
            {
                routeData.Values.Add("statusCode", httpException.GetHttpCode());
            }

            IController controller = new ErrorController();
            var wrapper = new HttpContextWrapper(this.Context);
            var context = new RequestContext(wrapper, routeData);
            controller.Execute(context);
            Response.End();
        }
    }
}
