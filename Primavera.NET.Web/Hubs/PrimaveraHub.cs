﻿namespace Primavera.NET.Web.Hubs
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Policy;
    using System.Threading;
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR;

    using Primavera.NET.Data.Models;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;
    using Primavera.NET.Localization;

    #endregion

    [SignalRAuthorize(Roles = "User, Admin")]
    public class PrimaveraHub : Hub<IPrimaveraClientProxy>
    {
        #region Private variables
        private static readonly object HubUsersLock = new object();

        private static readonly object UnreadMessagesLock = new object();

        private readonly IUnitOfWork unitOfWork;

        private readonly IMapper mapper;
        #endregion

        #region Constructors

        static PrimaveraHub()
        {
            UnreadMessages = new Dictionary<int, Queue<HubMessage>>();
        }

        public PrimaveraHub(IUnitOfWork unitOfWork, IMapper mapper, IEventsBus eventBus)
        {
            ValidationHelper.IsNotNull(unitOfWork, "unitOfWork");
            ValidationHelper.IsNotNull(mapper, "mapper");
            ValidationHelper.IsNotNull(eventBus, "eventBus");

            this.unitOfWork = unitOfWork;
            this.mapper = mapper;

            eventBus.OnUserDisconnectRequested += this.EventBus_OnUserDisconnectRequested;
            eventBus.OnUsersListReloadRequested += this.EventBus_OnUsersListReloadRequested;

            this.MyLoadHubUsersFirstTimeSafe();
        }

        #endregion

        #region Properties

        protected static Dictionary<int, HubUser> HubUsers { get; private set; }

        protected static Dictionary<int, Queue<HubMessage>> UnreadMessages { get; private set; }

        protected virtual IPrimaveraClientProxy Caller
        {
            get
            {
                return this.Clients.Caller;
            }
        }

        protected virtual IPrimaveraClientProxy All
        {
            get
            {
                return this.Clients.All;
            }
        }

        protected CustomIdentity CurrentIdentity
        {
            get
            {
                return this.Context.User.Identity as CustomIdentity;
            }
        }
        #endregion

        #region Methods (service)

        public void SetStatus(string statusMessage)
        {
            // Update user in database.
            var userModel = this.unitOfWork.Users.GetBy(this.CurrentIdentity.UserId);
            userModel.StatusMessage = statusMessage;
            this.unitOfWork.Users.Update(userModel);
            this.unitOfWork.SaveChanges();

            // Update hub user.
            HubUser hubUser = this.MyFindUserSafe(this.CurrentIdentity.UserId);
            hubUser.StatusMessage = statusMessage;

            // Notify users.
            this.Clients.AllExcept(hubUser.ConnectionId).ReceivedUserChanged(hubUser);
        }

        public void GetUnreadMessages()
        {
            var messages = this.MyDequeueUndreadMessagesSafe(this.CurrentIdentity.UserId);
            while (messages.Any())
            {
                this.Clients.Caller.ReceivedMessage(messages.Dequeue());
            }
        }

        public object GetChatMessages(int count, int? beforeMessageId)
        {
            IEnumerable<MessageModel> messages;
            if (beforeMessageId != null)
            {
                messages = this.unitOfWork.Messages.GetChatMessages(count, beforeMessageId.Value);
                return this.MyMapMessages(messages);
            }

            messages = this.unitOfWork.Messages.GetChatMessages(count);
            this.Caller.ReceivedChatMessages(this.MyMapMessages(messages));
            return null;
        }

        public object GetPrivateMessages(int userId1, int userId2, int count, int? beforeMessageId)
        {
            IEnumerable<MessageModel> messages;

            if (beforeMessageId != null)
            {
                messages = this.unitOfWork.Messages.GetPrivateMessaegs(userId1, userId2, count, beforeMessageId.Value);
            }
            else
            {
                messages = this.unitOfWork.Messages.GetPrivateMessages(userId1, userId2, count);
            }

            return this.MyMapMessages(messages);
        }

        public void GetUsers()
        {
            this.Caller.ReceivedUsersList(this.MyGetAllUsersSafe());
        }

        public object GetResources()
        {
            Type type = typeof(Strings);
            var properties = type.GetProperties().Where(p => p.PropertyType == typeof(string));

            return properties.ToDictionary(g => g.Name, g => g.GetValue(null, null).ToString());
        }

        public void SendMessage(string message, int? recipientUserId)
        {
            MessageModel messageModel = this.unitOfWork.Messages.CreateNew();
            messageModel.Content = message;
            messageModel.SenderUserId = this.CurrentIdentity.UserId;
            messageModel.RecipientUserId = recipientUserId;
            messageModel.CreatedAt = DateTime.Now;
            this.unitOfWork.Messages.Add(messageModel);
            this.unitOfWork.SaveChanges();

            HubMessage hubMessage = this.mapper.Map<HubMessage>(messageModel);
            hubMessage.Nickname = this.CurrentIdentity.Nickname;

            if (recipientUserId == null)
            {
                this.Clients.All.ReceivedMessage(hubMessage);
            }
            else
            {
                HubUser recipentUser = this.MyFindUserSafe(recipientUserId.Value);
                HubUser currentUser = this.MyFindUserSafe(this.CurrentIdentity.UserId);

                if (!string.IsNullOrEmpty(recipentUser.ConnectionId))
                {
                    this.Clients.Client(recipentUser.ConnectionId).ReceivedMessage(hubMessage);
                }
                else
                {
                    this.MyEnqueueUnreadMessageSafe(recipentUser.Id, hubMessage);
                }

                this.Caller.ReceivedMessage(hubMessage);
            }
        }

        public object GetCurrentUser()
        {
            return this.MyFindUserSafe(this.CurrentIdentity.UserId);
        }

        public override Task OnConnected()
        {
            HubUser user = this.MyFindUserSafe(this.CurrentIdentity.UserId);
            user.ConnectionId = this.Context.ConnectionId;

            if (user.Status != HubUserStatus.Available)
            {
                user.Status = HubUserStatus.Available;
                this.Clients.AllExcept(user.ConnectionId).ReceivedUserChanged(user);
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            HubUser user = this.MyFindUserSafe(this.CurrentIdentity.UserId);
            user.ConnectionId = null;

            if (user.Status != HubUserStatus.Unavailable)
            {
                user.Status = HubUserStatus.Unavailable;
                this.Clients.All.ReceivedUserChanged(user);
            }

            return base.OnDisconnected(stopCalled);
        }

        #endregion

        #region Event handlers
        private void EventBus_OnUserDisconnectRequested(object sender, OnUserDisconnectRequestedEventArgs e)
        {
            HubUser user = this.MyFindUserSafe(e.UserId);

            if (user.Id == this.CurrentIdentity.UserId && !string.IsNullOrEmpty(user.ConnectionId))
            {
                this.Clients.Caller.ReceivedDisconnectRequest();
            }
        }

        private void EventBus_OnUsersListReloadRequested(object sender, EventArgs e)
        {            
            this.MyReloadHubUsersSafe();

            this.Clients.Caller.ReceivedUsersList(HubUsers.Values.ToList());
        }
        #endregion

        #region My methods

        private void MyEnqueueUnreadMessageSafe(int userId, HubMessage message)
        {
            lock (UnreadMessagesLock)
            {
                Queue<HubMessage> queue;

                if (!UnreadMessages.TryGetValue(userId, out queue))
                {
                    queue = new Queue<HubMessage>();
                    UnreadMessages[userId] = queue;
                }

                queue.Enqueue(message);
            }
        }

        private Queue<HubMessage> MyDequeueUndreadMessagesSafe(int userId)
        {
            lock (UnreadMessagesLock)
            {
                Queue<HubMessage> queue;

                if (!UnreadMessages.TryGetValue(userId, out queue))
                {
                    queue = new Queue<HubMessage>();
                    UnreadMessages[userId] = queue;
                }

                var result = new Queue<HubMessage>(queue);

                queue.Clear();

                return result;
            }
        }

        private IEnumerable<HubUser> MyGetAllUsersSafe()
        {
            lock (HubUsersLock)
            {
                return HubUsers.Select(g => g.Value).ToList();
            }
        }

        private void MyLoadHubUsersFirstTimeSafe()
        {
            if (HubUsers == null)
            {
                lock (HubUsersLock)
                {
                    if (HubUsers == null)
                    {
                        var allUsers = this.unitOfWork.Users.GetAll();
                        HubUsers = this.mapper.Map<UserModel, HubUser>(allUsers).ToDictionary(g => g.Id, g => g);
                    }
                }
            }
        }

        private void MyReloadHubUsersSafe()
        {
            try
            {
                if (Monitor.TryEnter(HubUsersLock))
                {
                    var backup = new List<HubUser>();

                    if (HubUsers != null)
                    {
                        backup.AddRange(HubUsers.Values);
                    }

                    HubUsers = null;

                    var allUsers = this.unitOfWork.Users.GetAll();
                    HubUsers = this.mapper.Map<UserModel, HubUser>(allUsers).ToDictionary(g => g.Id, g => g);

                    IDictionary<int, HubUser> oldHubUsers = backup.ToDictionary(g => g.Id, g => g);

                    foreach (var loopOldHubUser in oldHubUsers)
                    {
                        HubUser newUser;

                        if (HubUsers.TryGetValue(loopOldHubUser.Key, out newUser))
                        {
                            // Restore some informations.
                            newUser.Status = loopOldHubUser.Value.Status;
                            newUser.ConnectionId = loopOldHubUser.Value.ConnectionId;
                        }
                    }
                }
            }
            finally
            {
                Monitor.Exit(HubUsersLock);
            }
        }

        private IEnumerable<HubMessage> MyMapMessages(IEnumerable<MessageModel> messageModels)
        {
            var mappedMessages = this.mapper.Map<MessageModel, HubMessage>(messageModels);

            foreach (var loopMessage in mappedMessages)
            {
                loopMessage.Nickname = this.MyFindUserSafe(loopMessage.SenderUserId).Nickname;
            }

            return mappedMessages;
        }

        private HubUser MyFindUserSafe(int userId)
        {
            HubUser user;

            lock (HubUsersLock)
            {
                HubUsers.TryGetValue(userId, out user);

                if (user == null)
                {
                    UserModel userModel = this.unitOfWork.Users.GetBy(userId);

                    if (userModel != null)
                    {
                        user = this.mapper.Map<HubUser>(userModel);
                    }
                }

                HubUsers[userId] = user;
            }

            return user;
        }
        #endregion
    }
}