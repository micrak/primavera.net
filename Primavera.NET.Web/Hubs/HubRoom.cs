﻿namespace Primavera.NET.Web.Hubs
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    #endregion

    public class HubRoom
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}