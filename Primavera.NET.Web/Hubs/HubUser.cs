﻿namespace Primavera.NET.Web.Hubs
{
    #region Usings
    using System.Collections.Generic;

    using Newtonsoft.Json;

    using Primavera.NET.Data.Models; 
    #endregion

    public class HubUser
    {
        #region Constructors
        public HubUser()
        {
            this.Status = HubUserStatus.Unavailable;
        }
        #endregion

        #region Properties
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("nickname")]
        public string Nickname { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("roles")]
        public IList<RoleType> Roles { get; set; }

        [JsonIgnore]
        public string ConnectionId { get; set; }
        
        [JsonProperty("statusCode")]
        public HubUserStatus Status { get; set; }
        
        [JsonProperty("status")]
        public string StatusString
        {
            get
            {
                return this.Status.ToString();
            }
        }

        [JsonProperty("statusMessage")]
        public string StatusMessage { get; set; }
        #endregion
    }
}