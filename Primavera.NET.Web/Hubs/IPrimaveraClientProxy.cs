﻿namespace Primavera.NET.Web.Hubs
{
    #region Usings
    using System.Collections.Generic;

    using Primavera.NET.Data.Models;
    #endregion

    public interface IPrimaveraClientProxy
    {
        void ReceivedDisconnectRequest();

        void ReceivedUsersList(IEnumerable<HubUser> users);

        void ReceivedChatMessages(IEnumerable<HubMessage> messages);

        void ReceivedMessage(HubMessage message);

        void ReceivedUserChanged(HubUser user);
    }
}
