﻿namespace Primavera.NET.Web.Hubs
{
    #region Usings
    using System;

    using Newtonsoft.Json;

    #endregion

    public class HubMessage
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("senderUserId")]
        public int SenderUserId { get; set; }

        [JsonProperty("recipientUserId")]
        public int? RecipientUserId { get; set; }

        [JsonProperty("nickname")]
        public string Nickname { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
    }
}