﻿namespace Primavera.NET.Web.Hubs
{
    public enum HubUserStatus
    {
        Available = 0,
        Unavailable = 1
    }
}