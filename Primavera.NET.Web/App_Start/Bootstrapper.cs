﻿namespace Primavera.NET.Web
{
    #region Usings    

    using System.Web;
    using System.Web.Hosting;

    using Autofac;

    using Microsoft.AspNet.SignalR;

    using Owin;

    using Primavera.NET.Data.EntityFramework.UnitOfWork;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;

    #endregion

    public static class Bootstrapper
    {
        private static IContainer autofacContainer;

        public static void Start(IAppBuilder appBuilder)
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance<IUnitOfWork>(new EntityFrameworkUnitOfWork());
            builder.RegisterType<DefaultAuthorizationService>().As<IAuthorizationService>();
            builder.RegisterType<AutoMapperMapper>().As<IMapper>();
            builder.RegisterType<DefaultNotificationService>().As<INotificationService>();
            builder.RegisterType<EventsBus>().As<IEventsBus>();

            Autofac.Integration.Mvc.RegistrationExtensions.RegisterControllers(builder, typeof(Bootstrapper).Assembly);
            Autofac.Integration.SignalR.RegistrationExtensions.RegisterHubs(builder, typeof(Bootstrapper).Assembly);

            autofacContainer = builder.Build();

            var mvcDependencyResolver = new Autofac.Integration.Mvc.AutofacDependencyResolver(autofacContainer);
            System.Web.Mvc.DependencyResolver.SetResolver(mvcDependencyResolver);

            var signalRDependencyResolver = new Autofac.Integration.SignalR.AutofacDependencyResolver(autofacContainer);
            appBuilder.MapSignalR(new HubConfiguration() { Resolver = signalRDependencyResolver });          
        }
    }
}