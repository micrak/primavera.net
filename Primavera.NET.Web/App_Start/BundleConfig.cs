﻿namespace Primavera.NET.Web
{
    #region Usings
    using System.Web.Optimization;
    #endregion

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js", 
                        "~/Scripts/jquery.scrollTo.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-notify.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/signalR").Include(
                "~/Scripts/jquery.signalR-2.2.0.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                    "~/Scripts/angular.js",
                    "~/Scripts/angular-mocks.js",                    
                    "~/Scripts/angular-route.js",
                    "~/Scripts/angular-emoticons.js"));

            bundles.Add(new ScriptBundle("~/bundles/linqjs").Include("~/Scripts/linq.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-theme.css",
                      "~/Content/bootstrap-notify.css",
                      "~/Content/angular-emoticons.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/Site").Include(
                "~/MyScripts/Notifications.js"));

            bundles.Add(new ScriptBundle("~/bundles/PrimaveraApp").Include("~/MyScripts/PrimaveraApp.js"));
        }
    }
}
