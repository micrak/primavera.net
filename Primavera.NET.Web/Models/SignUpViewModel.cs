﻿namespace Primavera.NET.Web.Models
{
    #region Usings
    using System.ComponentModel.DataAnnotations; 
    #endregion

    public class SignUpViewModel : ViewModelBase
    {
        [Required]
        public string Nickname { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string ConfirmPassword { get; set; }
    }
}