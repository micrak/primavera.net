﻿namespace Primavera.NET.Web.Models
{
    #region Usings
    using System.ComponentModel.DataAnnotations;
    #endregion

    public class ChangeNicknameViewModel : ViewModelBase
    {
        #region Properties
        [Required(AllowEmptyStrings = false)]
        public string Nickname { get; set; }
        #endregion
    }
}