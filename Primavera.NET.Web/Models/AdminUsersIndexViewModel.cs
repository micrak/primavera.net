﻿namespace Primavera.NET.Web.Models
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using Primavera.NET.Framework;

    #endregion

    public class AdminUsersIndexViewModel : ViewModelBase
    {
        #region Constructors
        public AdminUsersIndexViewModel(IEnumerable<AdminUsersItemViewModel> users)
        {
            ValidationHelper.IsNotNull(users, "users");

            this.Users = users.ToList();
        }
        #endregion

        #region Properties
        public IEnumerable<AdminUsersItemViewModel> Users { get; private set; }
        #endregion
    }
}