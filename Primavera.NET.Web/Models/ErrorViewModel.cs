﻿namespace Primavera.NET.Web.Models
{
    #region Usings
    using System;
    #endregion

    public class ErrorViewModel
    {
        #region Constructors
        public ErrorViewModel()
        {
        }

        public ErrorViewModel(int statusCode, string message, Exception exception, bool isDeveloperMode)
        {
            this.StatusCode = statusCode;
            this.Message = message;
            this.Exception = exception;
            this.IsDeveloperMode = isDeveloperMode;
        }
        #endregion

        #region Properties
        public int StatusCode
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public Exception Exception
        {
            get;
            set;
        }

        public bool IsDeveloperMode
        {
            get;
            set;
        }
        #endregion
    }
}