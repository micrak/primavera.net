﻿namespace Primavera.NET.Web.Models
{
    #region Usings
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Primavera.NET.Data.Models;

    #endregion

    public class AdminUsersItemViewModel : ViewModelBase
    {
        #region Constructors
        public AdminUsersItemViewModel()
        {
            this.Roles = new List<RoleType>();
        }
        #endregion

        #region Properties
        public int Id { get; set; }

        public string Email { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Nickname { get; set; }

        [Required]
        public List<RoleType> Roles { get; set; }

        public List<RoleType> AvailableRoles
        {
            get
            {
                return new List<RoleType> { RoleType.Admin, RoleType.User };
            }
        }

        public string StatusMessage { get; set; }
        #endregion
    }
}