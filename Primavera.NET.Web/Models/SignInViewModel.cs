﻿namespace Primavera.NET.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    public class SignInViewModel : ViewModelBase
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}