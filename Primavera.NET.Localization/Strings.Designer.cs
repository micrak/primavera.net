﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.0
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Primavera.NET.Localization {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Primavera.NET.Localization.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Administrate.
        /// </summary>
        public static string StringAdministrate {
            get {
                return ResourceManager.GetString("StringAdministrate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Primavera.NET.
        /// </summary>
        public static string StringApplicationName {
            get {
                return ResourceManager.GetString("StringApplicationName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to delete user &apos;{0}&apos;.
        /// </summary>
        public static string StringAreYouSureYouWantToDeleteUserX {
            get {
                return ResourceManager.GetString("StringAreYouSureYouWantToDeleteUserX", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to At.
        /// </summary>
        public static string StringAt {
            get {
                return ResourceManager.GetString("StringAt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Authorization failed.
        /// </summary>
        public static string StringAuthorizationFailed {
            get {
                return ResourceManager.GetString("StringAuthorizationFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Available.
        /// </summary>
        public static string StringAvailable {
            get {
                return ResourceManager.GetString("StringAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad request.
        /// </summary>
        public static string StringBadRequest {
            get {
                return ResourceManager.GetString("StringBadRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string StringCancel {
            get {
                return ResourceManager.GetString("StringCancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change nickname.
        /// </summary>
        public static string StringChangeNickname {
            get {
                return ResourceManager.GetString("StringChangeNickname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change roles.
        /// </summary>
        public static string StringChangeRoles {
            get {
                return ResourceManager.GetString("StringChangeRoles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chat.
        /// </summary>
        public static string StringChat {
            get {
                return ResourceManager.GetString("StringChat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm password.
        /// </summary>
        public static string StringConfirmPassword {
            get {
                return ResourceManager.GetString("StringConfirmPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create.
        /// </summary>
        public static string StringCreate {
            get {
                return ResourceManager.GetString("StringCreate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create a new user.
        /// </summary>
        public static string StringCreateANewUser {
            get {
                return ResourceManager.GetString("StringCreateANewUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create room.
        /// </summary>
        public static string StringCreateRoom {
            get {
                return ResourceManager.GetString("StringCreateRoom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string StringDelete {
            get {
                return ResourceManager.GetString("StringDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string StringEdit {
            get {
                return ResourceManager.GetString("StringEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        public static string StringEmail {
            get {
                return ResourceManager.GetString("StringEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error.
        /// </summary>
        public static string StringError {
            get {
                return ResourceManager.GetString("StringError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The entity &apos;{0}&apos; has been not found.
        /// </summary>
        public static string StringErrorTheEntityXHasBeenNotFound {
            get {
                return ResourceManager.GetString("StringErrorTheEntityXHasBeenNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Internal server error.
        /// </summary>
        public static string StringInternalServerError {
            get {
                return ResourceManager.GetString("StringInternalServerError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Join.
        /// </summary>
        public static string StringJoin {
            get {
                return ResourceManager.GetString("StringJoin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Members.
        /// </summary>
        public static string StringMembers {
            get {
                return ResourceManager.GetString("StringMembers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string StringName {
            get {
                return ResourceManager.GetString("StringName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you a new user? Click here to create your account!.
        /// </summary>
        public static string StringNewUserClickHereToCreateYourAccount {
            get {
                return ResourceManager.GetString("StringNewUserClickHereToCreateYourAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nickname.
        /// </summary>
        public static string StringNickname {
            get {
                return ResourceManager.GetString("StringNickname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Page not found.
        /// </summary>
        public static string StringPageNotFound {
            get {
                return ResourceManager.GetString("StringPageNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        public static string StringPassword {
            get {
                return ResourceManager.GetString("StringPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passwords do not match.
        /// </summary>
        public static string StringPasswordsDoNotMatch {
            get {
                return ResourceManager.GetString("StringPasswordsDoNotMatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permission denied.
        /// </summary>
        public static string StringPermissionDenied {
            get {
                return ResourceManager.GetString("StringPermissionDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Private chat.
        /// </summary>
        public static string StringPrivateChat {
            get {
                return ResourceManager.GetString("StringPrivateChat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Resource not found.
        /// </summary>
        public static string StringResourceNotFound {
            get {
                return ResourceManager.GetString("StringResourceNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The resource you are looking for has been not found.
        /// </summary>
        public static string StringResourceYouAreLookingForHasBeenNotFound {
            get {
                return ResourceManager.GetString("StringResourceYouAreLookingForHasBeenNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Roles.
        /// </summary>
        public static string StringRoles {
            get {
                return ResourceManager.GetString("StringRoles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Room.
        /// </summary>
        public static string StringRoom {
            get {
                return ResourceManager.GetString("StringRoom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The room has been created.
        /// </summary>
        public static string StringRoomHasBeenCreated {
            get {
                return ResourceManager.GetString("StringRoomHasBeenCreated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rooms.
        /// </summary>
        public static string StringRooms {
            get {
                return ResourceManager.GetString("StringRooms", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string StringSave {
            get {
                return ResourceManager.GetString("StringSave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Send.
        /// </summary>
        public static string StringSend {
            get {
                return ResourceManager.GetString("StringSend", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show older.
        /// </summary>
        public static string StringShowOlder {
            get {
                return ResourceManager.GetString("StringShowOlder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sign In.
        /// </summary>
        public static string StringSignIn {
            get {
                return ResourceManager.GetString("StringSignIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sign Out.
        /// </summary>
        public static string StringSignOut {
            get {
                return ResourceManager.GetString("StringSignOut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sign Up.
        /// </summary>
        public static string StringSignUp {
            get {
                return ResourceManager.GetString("StringSignUp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some fields are not filled correctly.
        /// </summary>
        public static string StringSomeFieldsAreNotFilledCorrectly {
            get {
                return ResourceManager.GetString("StringSomeFieldsAreNotFilledCorrectly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status code.
        /// </summary>
        public static string StringStatusCode {
            get {
                return ResourceManager.GetString("StringStatusCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status message.
        /// </summary>
        public static string StringStatusMessage {
            get {
                return ResourceManager.GetString("StringStatusMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tabs.
        /// </summary>
        public static string StringTabs {
            get {
                return ResourceManager.GetString("StringTabs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There were unexpected problems with the connection. Please wait a moment or come back later.
        /// </summary>
        public static string StringThereWereProblemsWithConnection {
            get {
                return ResourceManager.GetString("StringThereWereProblemsWithConnection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user has been changed successfully.
        /// </summary>
        public static string StringTheUserHasBeenChanged {
            get {
                return ResourceManager.GetString("StringTheUserHasBeenChanged", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user has been deleted.
        /// </summary>
        public static string StringTheUserHasBeenDeleted {
            get {
                return ResourceManager.GetString("StringTheUserHasBeenDeleted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The value &apos;{0}&apos; is incorrect.
        /// </summary>
        public static string StringTheValueXIsIncorrect {
            get {
                return ResourceManager.GetString("StringTheValueXIsIncorrect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This nickname is not unique.
        /// </summary>
        public static string StringThisNicknameIsNotUnique {
            get {
                return ResourceManager.GetString("StringThisNicknameIsNotUnique", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unavailable.
        /// </summary>
        public static string StringUnavailable {
            get {
                return ResourceManager.GetString("StringUnavailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unexpected exception.
        /// </summary>
        public static string StringUnexpectedException {
            get {
                return ResourceManager.GetString("StringUnexpectedException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Users management.
        /// </summary>
        public static string StringUsersManagement {
            get {
                return ResourceManager.GetString("StringUsersManagement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user with email &apos;{0}&apos; or nickname &apos;{1}&apos; already exists.
        /// </summary>
        public static string StringUserWithEmailXOrNicknameXAlreadyExists {
            get {
                return ResourceManager.GetString("StringUserWithEmailXOrNicknameXAlreadyExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You cannot change your &apos;{0}&apos; role.
        /// </summary>
        public static string StringYouCannotChangeYourXRole {
            get {
                return ResourceManager.GetString("StringYouCannotChangeYourXRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You cannot delete yourself.
        /// </summary>
        public static string StringYouCannotDeleteYourSelf {
            get {
                return ResourceManager.GetString("StringYouCannotDeleteYourSelf", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The account has been created.
        /// </summary>
        public static string StringYourAccountHasBeenCreated {
            get {
                return ResourceManager.GetString("StringYourAccountHasBeenCreated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your nickname has been changed successfully.
        /// </summary>
        public static string StringYourNicknameHasBeenChangedSuccessfully {
            get {
                return ResourceManager.GetString("StringYourNicknameHasBeenChangedSuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your status has been changed successfully.
        /// </summary>
        public static string StringYourStatusHasBeenChangedSuccessFully {
            get {
                return ResourceManager.GetString("StringYourStatusHasBeenChangedSuccessFully", resourceCulture);
            }
        }
    }
}
