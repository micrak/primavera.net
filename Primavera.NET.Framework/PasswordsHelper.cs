﻿namespace Primavera.NET.Framework
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    public static class PasswordsHelper
    {
        #region Constants
        private const string ConstSalt = "981W4EIFUHO495QO23WFWAHGOA7SY21H23";
        #endregion

        #region Methods
        public static string Hash(string password)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(password + ConstSalt);

            HashAlgorithm hashAlgorithm = new SHA512CryptoServiceProvider();
            byte[] hash = hashAlgorithm.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        } 
        #endregion
    }
}
