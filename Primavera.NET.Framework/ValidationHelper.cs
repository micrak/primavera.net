﻿namespace Primavera.NET.Framework
{
    #region Using
    using System;
    #endregion

    public static class ValidationHelper
    {
        #region Methods
        public static void IsNotNull([ValidatedNotNullAttribute] object item, string parameterName)
        {
            if (item == null)
            {
                throw new ArgumentNullException(parameterName);
            }
        }        
        #endregion
    }
}