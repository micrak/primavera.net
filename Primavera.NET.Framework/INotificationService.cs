﻿namespace Primavera.NET.Framework
{
    using System.Collections.Generic;

    public interface INotificationService
    {
        void Error(string message);

        void Information(string message);

        IEnumerable<Notification> GetNotifications(bool clear);
    }
}
