﻿namespace Primavera.NET.Framework
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    #endregion

    public static class IQueryableExtensions
    {
        #region Methods

        public static IQueryable<T> GetLast<T>(this IOrderedQueryable<T> queryable, int count, int elementsCount)
        {
            ValidationHelper.IsNotNull(queryable, "queryable");
            return queryable.Skip(Math.Max(0, elementsCount - count)).Take(count);
        }
        #endregion
    }
}
