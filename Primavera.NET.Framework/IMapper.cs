﻿namespace Primavera.NET.Framework
{
    using System.Collections;
    using System.Collections.Generic;

    public interface IMapper
    {
        TDestination Map<TSource, TDestination>(TSource source);

        TDestination Map<TDestination>(object source);

        IEnumerable<TDestination> Map<TSource, TDestination>(IEnumerable<TSource> source);
    }
}
