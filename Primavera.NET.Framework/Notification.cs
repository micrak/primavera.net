﻿namespace Primavera.NET.Framework
{
    public class Notification
    {
        public NotificationType NotificationType { get; set; }

        public string Message { get; set; }
    }
}
