﻿namespace Primavera.NET.Framework
{
    #region Usings
    using System; 
    #endregion

    [Serializable]
    public class GeneralException : Exception
    {
        #region Constructors
        public GeneralException()
        {
        }

        public GeneralException(string message)
            : base(message)
        {
        }

        public GeneralException(string message, Exception innerException)
            : base(message, innerException)
        {
        } 
        #endregion
    }
}
