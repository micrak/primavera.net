﻿namespace Primavera.NET.Framework
{
    public enum NotificationType
    {
        Error,
        Information
    }
}
