﻿namespace Primavera.NET.Framework
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper; 
    #endregion

    public class AutoMapperMapper : IMapper
    {
        #region Private variables
        private static readonly Dictionary<Type, HashSet<Type>> ExistingMappings = new Dictionary<Type, HashSet<Type>>();
        #endregion

        public TDestination Map<TSource, TDestination>(TSource source)
        {
            ValidationHelper.IsNotNull(source, "source");
            return this.Map<TDestination>(source);
        }

        public TDestination Map<TDestination>(object source)
        {
            ValidationHelper.IsNotNull(source, "source");

            Type sourceType = source.GetType();

            HashSet<Type> hashSet;

            if (!ExistingMappings.TryGetValue(sourceType, out hashSet))
            {
                hashSet = new HashSet<Type>();
                ExistingMappings[sourceType] = hashSet;
            }

            Type destinationType = typeof(TDestination);

            if (!hashSet.Contains(destinationType))
            {
                Mapper.CreateMap(sourceType, destinationType);
                hashSet.Add(destinationType);
            }

            return (TDestination)Mapper.Map(source, sourceType, destinationType);
        }

        public IEnumerable<TDestination> Map<TSource, TDestination>(IEnumerable<TSource> source)
        {
            ValidationHelper.IsNotNull(source, "source");
            return source.Select(g => this.Map<TSource, TDestination>(g)).ToList();
        }
    }
}
