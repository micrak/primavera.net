﻿namespace Primavera.NET.Framework
{
    #region Usings
    using System;
    using System.Globalization;
    #endregion

    public static class StringExtensions
    {
        #region Methods
        public static string Backslash(this string value)
        {
            return value.Backslash(false);
        }

        public static string Backslash(this string value, bool space)
        {
            return value.Suffix(@"\", space);
        }

        public static string BracketClose(this string value)
        {
            return value.BracketClose(false);
        }

        public static string BracketClose(this string value, bool space)
        {
            return value.Suffix(")", space);
        }

        public static string BracketOpen(this string value)
        {
            return value.BracketOpen(false);
        }

        public static string BracketOpen(this string value, bool space)
        {
            return value.Suffix("(", space);
        }

        public static string Colon(this string value, bool space)
        {
            return value.Suffix(":", space);
        }

        public static string Colon(this string value)
        {
            return value.Colon(false);
        }

        public static string Comma(this string value, bool space)
        {
            return value.Suffix(",", space);
        }

        public static string Comma(this string value)
        {
            return value.Comma(false);
        }

        public static bool Contains(this string value, string searchValue, StringComparison stringComparison)
        {
            if (value == null)
            {
                return false;
            }

            return value.IndexOf(searchValue, stringComparison) >= 0;
        }

        public static string Dot(this string value, bool space)
        {
            return value.Suffix(".", space);
        }

        public static string Dot(this string value)
        {
            return value.Dot(false);
        }

        public static string Dots(this string value, bool space)
        {
            return value.Suffix("...", space);
        }

        public static string Dots(this string value)
        {
            return value.Dots(false);
        }

        public static string EqualitySign(this string value)
        {
            return value.EqualitySign(false);
        }

        public static string EqualitySign(this string value, bool space)
        {
            return value.Suffix("=", space);
        }

        public static string FormatText(this string value, params object[] values)
        {
            return value.FormatText(CultureInfo.InvariantCulture, values);
        }

        public static string FormatText(this string value, IFormatProvider formatProvider, params object[] values)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            return (formatProvider == null) ? string.Format(CultureInfo.InvariantCulture, value, values) : string.Format(formatProvider, value, values);
        }

        public static string Hyphen(this string value)
        {
            return value.Hyphen(false);
        }

        public static string Hyphen(this string value, bool space)
        {
            return value.Suffix("-", space);
        }

        public static string Question(this string value)
        {
            return value.Question(false);
        }

        public static string Question(this string value, bool space)
        {
            return value.Suffix("?", space);
        }

        public static string RecBracketClose(this string value)
        {
            return value.RecBracketClose(false);
        }

        public static string RecBracketClose(this string value, bool space)
        {
            return value.Suffix("]", space);
        }

        public static string RecBracketOpen(this string value)
        {
            return value.RecBracketOpen(false);
        }

        public static string RecBracketOpen(this string value, bool space)
        {
            return value.Suffix("[", space);
        }

        public static string Semicolon(this string value, bool space)
        {
            return value.Suffix(";", space);
        }

        public static string Semicolon(this string value)
        {
            return value.Semicolon(false);
        }

        public static string Slash(this string value)
        {
            return value.Slash(false);
        }

        public static string Slash(this string value, bool space)
        {
            return value.Suffix("/", space);
        }

        public static string Space(this string value)
        {
            return value.Suffix(null, true);
        }

        public static string Suffix(this string value, string suffixValue, bool space)
        {
            if (value == null)
            {
                return null;
            }

            return value + (suffixValue ?? string.Empty) + (space ? " " : string.Empty);
        }

        public static string Underscore(this string value)
        {
            return value.Underscore(false);
        }

        public static string Underscore(this string value, bool space)
        {
            return value.Suffix("_", space);
        }
        #endregion
    }
}