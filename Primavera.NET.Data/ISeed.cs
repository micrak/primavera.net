﻿namespace Primavera.NET.Data
{
    using Primavera.NET.Data.UnitOfWork;

    public interface ISeed
    {
        void Begin();
    }
}
