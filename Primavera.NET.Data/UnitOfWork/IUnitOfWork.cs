﻿namespace Primavera.NET.Data.UnitOfWork
{    
    public interface IUnitOfWork
    {
        IUsersRepository Users { get; }

        IRolesRepository Roles { get; }

        IMessagesRepository Messages { get; }

        void SaveChanges();
    }
}
