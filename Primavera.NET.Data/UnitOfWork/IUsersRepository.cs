﻿namespace Primavera.NET.Data.UnitOfWork
{
    #region Usings
    using System.Collections.Generic;
    using Primavera.NET.Data.Models; 
    #endregion

    public interface IUsersRepository : IRepository<UserModel>
    {
        #region Methods

        UserModel GetBy(string email, string password);

        IList<UserModel> GetBy(IEnumerable<int> ids);

        UserModel GetBy(int id);

        UserModel GetByEmailOrNickname(string email, string nickname);

        void AddRoles(int userId, IEnumerable<RoleType> roles);

        IList<UserModel> GetAll();

        #endregion
    }
}
