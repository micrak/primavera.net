﻿namespace Primavera.NET.Data.UnitOfWork
{
    #region Usings
    using System.Collections.Generic;
    using Primavera.NET.Data.Models; 
    #endregion

    public interface IRolesRepository : IRepository<RoleModel>
    {
        #region Methods
        IList<RoleModel> GetBy(int userId);

        IDictionary<int, IList<RoleModel>> GetBy(IEnumerable<int> userIds);

        void RemoveUserFromAllRoles(int userid);

        #endregion
    }
}
