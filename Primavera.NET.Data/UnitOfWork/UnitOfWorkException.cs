﻿namespace Primavera.NET.Data.UnitOfWork
{
    #region Usings
    using System; 
    using Primavera.NET.Framework;    
    #endregion

    public class UnitOfWorkException : GeneralException
    {
        #region Constructors
        public UnitOfWorkException()
        {
        }

        public UnitOfWorkException(string message)
            : base(message)
        {
        }

        public UnitOfWorkException(string message, Exception innerException)
            : base(message, innerException)
        {
        } 
        #endregion
    }
}
