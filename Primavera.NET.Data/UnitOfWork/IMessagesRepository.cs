﻿namespace Primavera.NET.Data.UnitOfWork
{
    #region Usings

    using System.Collections.Generic;

    using Primavera.NET.Data.Models;
    #endregion

    public interface IMessagesRepository : IRepository<MessageModel>
    {
        IList<MessageModel> GetPrivateMessages(int userId1, int userId2, int count);

        IList<MessageModel> GetPrivateMessaegs(int userId, int userId2, int count, int beforeMessageId);

        IList<MessageModel> GetChatMessages(int count);

        IList<MessageModel> GetChatMessages(int count, int beforeMessageId);
    }
}
