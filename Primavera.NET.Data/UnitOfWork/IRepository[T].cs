﻿namespace Primavera.NET.Data.UnitOfWork
{
    #region Usings
    using Primavera.NET.Data.Models; 
    #endregion

    public interface IRepository<T>
        where T : ModelBase
    {
        T CreateNew();

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);
    }
}
