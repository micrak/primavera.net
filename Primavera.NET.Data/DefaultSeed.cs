﻿namespace Primavera.NET.Data
{
    #region Usings

    using System.Collections;
    using System.Collections.Generic;

    using Primavera.NET.Data.Models;
    using Primavera.NET.Data.UnitOfWork;
    using Primavera.NET.Framework;

    #endregion

    public class DefaultSeed : ISeed
    {
        #region Private variables
        private readonly IUnitOfWork unitOfWork;
        #endregion

        #region Constructors
        public DefaultSeed(IUnitOfWork unitOfWork)
        {
            ValidationHelper.IsNotNull(unitOfWork, "unitOfWork");

            this.unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public virtual void Begin()
        {
            this.MyCreateRole(RoleType.Admin);
            this.MyCreateRole(RoleType.User);

            this.MyCreateUser("admin", "admin@primavera.net", "admin", new[] { RoleType.Admin, RoleType.User });
            this.MyCreateUser("test1", "test1@primavera.net", "test1", new[] { RoleType.User }, "It is a good day.");
            this.MyCreateUser("test2", "test2@primavera.net", "test2", new[] { RoleType.User }, "Never say never.");
            this.MyCreateUser("test3", "test3@primavera.net", "test3", new[] { RoleType.User });
            this.MyCreateUser("test4", "test4@primavera.net", "test4", new[] { RoleType.User }, "It's so cloudy :(");
            this.MyCreateUser("test5", "test5@primavera.net", "test5", new[] { RoleType.User });
            this.MyCreateUser("test6", "test6@primavera.net", "test6", new[] { RoleType.User }, "I am a hero!");
            this.MyCreateUser("test7", "test7@primavera.net", "test7", new[] { RoleType.User }, "Kill all feeders!");
        }
        #endregion

        #region Methods

        private void MyCreateRole(RoleType roleType)
        {
            RoleModel role = this.unitOfWork.Roles.CreateNew();
            role.RoleType = roleType;
            this.unitOfWork.Roles.Add(role);

            this.unitOfWork.SaveChanges();
        }

        private void MyCreateUser(string nickname, string email, string password, IEnumerable<RoleType> roles, string statusMessage = "")
        {
            UserModel user = this.unitOfWork.Users.CreateNew();
            user.Nickname = nickname;
            user.Email = email;
            user.Password = PasswordsHelper.Hash(password);
            user.StatusMessage = statusMessage;
            this.unitOfWork.Users.Add(user);
            this.unitOfWork.SaveChanges();

            this.unitOfWork.Users.AddRoles(user.Id, roles);
            this.unitOfWork.SaveChanges();
        }
        #endregion
    }
}
