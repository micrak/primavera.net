﻿namespace Primavera.NET.Data.Models
{
    #region Usigns
    using System;
    #endregion

    public class MessageModel : ModelBase
    {
        #region Properties
        public virtual int SenderUserId { get; set; }

        public virtual int? RecipientUserId { get; set; }

        public virtual string Content { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        #endregion
    }
}
