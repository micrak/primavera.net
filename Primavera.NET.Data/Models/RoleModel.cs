﻿namespace Primavera.NET.Data.Models
{
    public class RoleModel : ModelBase
    {
        public virtual RoleType RoleType { get; set; }
    }
}