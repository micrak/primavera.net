﻿namespace Primavera.NET.Data.Models
{
    public abstract class ModelBase
    {
        public virtual int Id { get; set; }
    }
}
