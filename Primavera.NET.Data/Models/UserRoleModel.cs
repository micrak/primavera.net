﻿namespace Primavera.NET.Data.Models
{
    public class UserRoleModel : ModelBase
    {
        public virtual int UserId { get; set; }

        public virtual int RoleId { get; set; }
    }
}
