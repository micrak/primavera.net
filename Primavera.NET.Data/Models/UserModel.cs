﻿namespace Primavera.NET.Data.Models
{    
    public class UserModel : ModelBase
    {
        public virtual string Email { get; set; }

        public virtual string Password { get; set; }

        public virtual string Nickname { get; set; }

        public virtual string StatusMessage { get; set; }
    }
}
